package util

import (
	"reflect"
	"testing"
	"time"
)

func TestUpdateStructFieldsByMap(t *testing.T) {
	now := time.Now()
	nowParsed, _ := time.Parse(time.RFC3339Nano, now.Format(time.RFC3339Nano))
	type subStruct struct {
		Category string `json:"category"`
		Count    uint   `json:"count"`
	}
	type testStruct struct {
		Name      string      `json:"name"`
		Count     uint        `json:"count"`
		Sub       subStruct   `json:"sub"`
		Subs      []subStruct `json:"subs"`
		SubNoJson subStruct
		Start     time.Time  `json:"start"`
		End       *time.Time `json:"end"`
	}
	type args struct {
		inputStruct interface{}
		data        map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "TestUpdateStructFieldsByMap",
			args: args{
				inputStruct: testStruct{},
				data:        map[string]interface{}{"name": "test", "SubNoJson": map[string]interface{}{"category": "test"}, "subs": "", "start": now.Format(time.RFC3339Nano), "end": now.Format(time.RFC3339Nano)},
			},
			want: testStruct{
				Name: "test",
				SubNoJson: subStruct{
					Category: "test",
				},
				Start: nowParsed,
				End:   &nowParsed,
			},
			wantErr: false,
		},
		{
			name: "TestUpdateStructFieldsByMapPtr",
			args: args{
				inputStruct: testStruct{},
				data:        map[string]interface{}{"start": &now, "end": now},
			},
			want: testStruct{
				Start: now,
				End:   &now,
			},
			wantErr: false,
		},
		{
			name: "TestUpdateStructFieldsByMapInvalidInput",
			args: args{
				inputStruct: map[string]interface{}{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "TestUpdateStructFieldsByMapPanic",
			args: args{
				inputStruct: testStruct{},
				data: map[string]interface{}{
					"count": "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "TestUpdateStructFieldsByMapSubstructPanic",
			args: args{
				inputStruct: testStruct{},
				data: map[string]interface{}{
					"sub": map[string]interface{}{
						"count": "test",
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := UpdateStructFieldsByMap(tt.args.inputStruct, tt.args.data)
			if LastError != nil != tt.wantErr {
				t.Errorf("UpdateStructFieldsByMap() error = %v, wantErr %v", LastError, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateStructFieldsByMap() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsTypeAllowed(t *testing.T) {
	type args struct {
		fieldType reflect.Kind
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "TestIsTypeAllowed",
			args: args{
				fieldType: reflect.String,
			},
			want: true,
		},
		{
			name: "TestIsTypeAllowedFalse",
			args: args{
				fieldType: reflect.Slice,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsTypeAllowed(tt.args.fieldType); got != tt.want {
				t.Errorf("IsTypeAllowed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTimeFromMapField(t *testing.T) {
	now := time.Now()
	nowParsed, _ := time.Parse(time.RFC3339Nano, now.Format(time.RFC3339Nano))
	type args struct {
		f interface{}
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{
			name: "TestGetTimeFromMapFieldString",
			args: args{
				f: now.Format(time.RFC3339Nano),
			},
			want: nowParsed,
		},
		{
			name: "TestGetTimeFromMapFieldStringInvalid",
			args: args{
				f: "aa",
			},
			want: time.Time{},
		},
		{
			name: "TestGetTimeFromMapFieldPtr",
			args: args{
				f: &now,
			},
			want: now,
		},
		{
			name: "TestGetTimeFromMapField",
			args: args{
				f: now,
			},
			want: now,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetTimeFromMapField(tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTimeFromMapField() = %v, want %v", got, tt.want)
			}
		})
	}
}
