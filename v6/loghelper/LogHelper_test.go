package loghelper

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLogError(t *testing.T) {
	type args struct {
		fields logrus.Fields
		err    error
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestLogError",
			args: args{
				fields: logrus.Fields{},
				err:    errors.New("test"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			LogError(tt.args.fields, tt.args.err)
			assert.True(t, true)
		})
	}
}

func TestServer_PrepareLogger(t *testing.T) {
	type args struct {
		Level string
		Path  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "TestPrepareLoggerDebug",
			args: args{
				Level: "DEBUG",
				Path:  "/tmp/abc",
			},
			wantErr: false,
		},
		{
			name: "TestPrepareLoggerInfo",
			args: args{
				Level: "INFO",
				Path:  "/dev/stdout",
			},
			wantErr: false,
		},
		{
			name: "TestPrepareLoggerWarn",
			args: args{
				Level: "WARN",
				Path:  "/dev/stdout",
			},
			wantErr: false,
		},
		{
			name: "TestPrepareLoggerError",
			args: args{
				Level: "ERROR",
				Path:  "/dev/stdout",
			},
			wantErr: false,
		},
		{
			name: "TestPrepareLoggerDebugInvalidPath",
			args: args{
				Level: "DEBUG",
				Path:  "",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Set("Log.Level", tt.args.Level)
			viper.Set("Log.Path", tt.args.Path)
			if err := PrepareLogger(echo.New()); (err != nil) != tt.wantErr {
				t.Errorf("PrepareLogger() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
