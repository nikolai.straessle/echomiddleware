package async

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/ThreeDotsLabs/watermill"
	wSql "github.com/ThreeDotsLabs/watermill-sql/pkg/sql"
	"github.com/ThreeDotsLabs/watermill/message"
	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/nikolai.straessle/echomiddleware/v7/db"
	"gorm.io/gorm"
	"os"
)

const errorChannelName = "errors"

type AsyncWorker struct {
	asyncDb *sql.DB
	AppName string
}

func (w AsyncWorker) Init(driverName, dsn, appName string) (Async, error) {
	w.AppName = appName
	dialect, err := db.CreateSQLDB(driverName, dsn)
	if nil != err {
		return AsyncWorker{}, errors.WithStack(err)
	}
	gormDb, err := gorm.Open(
		dialect,
	)
	if nil != err {
		return AsyncWorker{}, err
	}
	sqlDB, err := gormDb.DB()
	if nil != err {
		return AsyncWorker{}, errors.WithStack(err)
	}
	w.asyncDb = sqlDB
	return w, nil
}

func (w AsyncWorker) Subscribe(topicName string, processMsgFunction func(event *message.Message) error, ctx context.Context) error {
	subscriber, err := wSql.NewSubscriber(
		w.asyncDb,
		wSql.SubscriberConfig{
			SchemaAdapter:    wSql.DefaultMySQLSchema{},
			OffsetsAdapter:   wSql.DefaultMySQLOffsetsAdapter{},
			InitializeSchema: true,
			ConsumerGroup:    w.AppName,
		},
		watermill.NewStdLogger(false, false),
	)
	if err != nil {
		return errors.WithStack(err)
	}

	messages, err := subscriber.Subscribe(ctx, topicName)
	if err != nil {
		return errors.WithStack(err)
	}
	go func() {
		for msg := range messages {
			if err := processMsgFunction(msg); nil != err {
				if err := w.PublishErrorMessage(msg, err); nil != err {
					logrus.WithFields(logrus.Fields{
						"uuid":     msg.UUID,
						"channel":  topicName,
						"metadata": msg.Metadata,
						"error":    err,
					}).Debug("failed to publish error message")
				}
			}
			msg.Ack()
		}
	}()
	return nil
}

func (w AsyncWorker) GetTagFromQueueMessage(msg *message.Message, tagName string) string {
	return msg.Metadata.Get(tagName)
}

func (w AsyncWorker) Publish(topicName string, payloadInterface interface{}, tenantID uint, messageType, action string, metadata map[string]string) error {
	payload, err := json.Marshal(payloadInterface)
	if nil != err {
		return errors.WithStack(err)
	}
	publisher, err := w.CreatePublisher()
	if nil != err {
		return errors.WithStack(err)
	}
	msg := message.NewMessage(watermill.NewUUID(), payload)
	msg.Metadata.Set("x-tenant-id", fmt.Sprintf("%d", tenantID))
	if nil == metadata {
		metadata = map[string]string{}
	}
	metadata["type"] = messageType
	metadata["action"] = action
	hasHeritage := true
	heritageNumber := 0
	for hasHeritage {
		if _, ok := metadata[fmt.Sprintf("heritage_%d", heritageNumber)]; ok {
			heritageNumber++
			continue
		}
		hasHeritage = false
		metadata[fmt.Sprintf("heritage_%d", heritageNumber)] = os.Getenv("APP_NAME")
	}
	hasTarget := true
	targetNumber := 0
	for hasTarget {
		if _, ok := metadata[fmt.Sprintf("target_%d", targetNumber)]; ok {
			targetNumber++
			continue
		}
		hasTarget = false
		metadata[fmt.Sprintf("target_%d", targetNumber)] = topicName
	}
	for k, v := range metadata {
		msg.Metadata.Set(k, v)
	}
	logrus.WithFields(logrus.Fields{
		"msg_id":   msg.UUID,
		"channel":  topicName,
		"metadata": msg.Metadata,
	}).Info("publish msg")
	if err := publisher.Publish(topicName, msg); nil != err {
		return errors.WithStack(err)
	}
	return nil
}

func (w AsyncWorker) CreatePublisher() (*wSql.Publisher, error) {
	publisher, err := wSql.NewPublisher(
		w.asyncDb,
		wSql.PublisherConfig{
			SchemaAdapter:        wSql.DefaultMySQLSchema{},
			AutoInitializeSchema: true,
		},
		watermill.NewStdLogger(false, false),
	)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return publisher, nil
}

func (w AsyncWorker) PublishErrorMessage(msg *message.Message, msgError error) error {
	if nil == msg || nil == msgError {
		return nil
	}
	logrus.WithFields(logrus.Fields{
		"msg_id":   msg.UUID,
		"metadata": msg.Metadata,
	}).Debug("publish error message")
	msg.Metadata.Set("error", msgError.Error())
	msg.Metadata.Set("error_app", os.Getenv("APP_NAME"))
	publisher, err := w.CreatePublisher()
	if nil != err {
		return errors.WithStack(err)
	}
	return publisher.Publish(errorChannelName, msg)
}
