package util

import (
	"github.com/pkg/errors"
	"reflect"
	"time"
)

var LastError error

func IsTypeAllowed(fieldType reflect.Kind) bool {
	switch fieldType {
	case reflect.Invalid, reflect.Uintptr, reflect.Array, reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.UnsafePointer, reflect.Ptr, reflect.Struct, reflect.Slice:
		return false
	default:
		return true
	}
}

func UpdateStructFieldsByMap(inputStruct interface{}, data map[string]interface{}) interface{} {
	LastError = nil
	defer func() {
		if r := recover(); r != nil {
			var err error
			switch x := r.(type) {
			case string:
				err = errors.New(x)
			case error:
				err = x
			default:
				err = errors.New("unknown panic")
			}
			LastError = err
		}
	}()
	v := reflect.ValueOf(&inputStruct).Elem()

	// Allocate a temporary variable with type of the struct.
	//    v.Elem() is the vale contained in the interface.
	tmp := reflect.New(v.Elem().Type()).Elem()

	// Copy the struct value contained in interface to
	// the temporary variable.
	tmp.Set(v.Elem())
	if tmp.Kind() != reflect.Struct {
		LastError = errors.New("invalid input type")
		return nil
	}
	for i := 0; i < tmp.Type().NumField(); i++ {
		jsonTag := tmp.Type().Field(i).Tag.Get("json")
		if "" == jsonTag {
			jsonTag = tmp.Type().Field(i).Name
		}
		f, ok := data[jsonTag]
		if ok {
			switch tmp.Type().Field(i).Type.Kind() {
			case reflect.Slice:
				continue
			case reflect.Struct:
				if subData, isMap := f.(map[string]interface{}); isMap {
					subInput := tmp.Field(i).Interface()
					updatedSubInput := UpdateStructFieldsByMap(subInput, subData)
					if nil != LastError {
						return nil
					}
					tmp.Field(i).Set(reflect.ValueOf(updatedSubInput))
				} else if "Time" == tmp.Type().Field(i).Type.Name() {
					t := GetTimeFromMapField(f)
					tmp.Field(i).Set(reflect.ValueOf(t))
				}
			case reflect.Ptr:
				if "*time.Time" == tmp.Type().Field(i).Type.String() {
					t := GetTimeFromMapField(f)
					if t.IsZero() {
						tmp.Field(i).Set(reflect.Zero(tmp.Type().Field(i).Type))
					} else {
						tmp.Field(i).Set(reflect.ValueOf(&t))
					}
				}
			default:
				if IsTypeAllowed(tmp.Field(i).Kind()) && IsTypeAllowed(reflect.ValueOf(f).Kind()) && tmp.Field(i).CanSet() {
					tmp.Field(i).Set(reflect.ValueOf(f).Convert(tmp.Field(i).Type()))
				}
			}
		}
	}
	v.Set(tmp)
	return inputStruct
}

func GetTimeFromMapField(f interface{}) time.Time {
	t := time.Time{}
	if timeString, ok := f.(string); ok {
		var err error
		t, err = time.Parse(time.RFC3339Nano, timeString)
		if nil != err {
			LastError = err
		}
	} else if timeTimePtr, ok := f.(*time.Time); ok {
		t = *timeTimePtr
	} else if timeTime, ok := f.(time.Time); ok {
		t = timeTime
	}
	return t
}
