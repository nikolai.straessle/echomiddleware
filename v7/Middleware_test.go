package echomiddleware

import (
	"errors"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHTTPErrorHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	reqHead := httptest.NewRequest(http.MethodHead, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	cHead := e.NewContext(reqHead, rec)
	internal := echo.NewHTTPError(http.StatusInternalServerError, errors.New("test"))
	internal = internal.SetInternal(echo.NewHTTPError(http.StatusInternalServerError, "test"))
	type args struct {
		err error
		c   echo.Context
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestHTTPErrorHandler",
			args: args{
				err: gorm.ErrRecordNotFound,
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerInternal",
			args: args{
				err: internal,
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerNoHTTPError",
			args: args{
				err: errors.New("test"),
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerHEAD",
			args: args{
				err: errors.New("test"),
				c:   cHead,
			},
		},
		{
			name: "TestHTTPErrorHandlerInvalidJSON",
			args: args{
				err: echo.NewHTTPError(http.StatusInternalServerError, "test", func() {}),
				c:   cHead,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			HTTPErrorHandler(tt.args.err, tt.args.c)
		})
	}
}

func TestAddRequestContextMiddleware(t *testing.T) {
	e := echo.New()
	db := &gorm.DB{}
	type args struct {
		e  *echo.Echo
		db *gorm.DB
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestAddRequestContextMiddleware",
			args: args{
				e:  e,
				db: db,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			AddRequestContextMiddleware(tt.args.e, tt.args.db)
		})
	}
}
