/*
 * Copyright (c) 2019 Nikolai Strässle.  All rights reserved.
 */

package db

import (
	"database/sql"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/nikolai.straessle/echomiddleware/v4"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"reflect"
)

var CurrentDatabase Database

type Database struct {
	DB          *gorm.DB
	Transaction *gorm.DB
}

func NewDatabase() (*Database, error) {
	dialect := mysql.New(mysql.Config{
		DriverName: viper.GetString("DB.Driver"),
		DSN:        viper.GetString("DB.DSN"),
	})
	db, err := gorm.Open(
		dialect,
		&gorm.Config{
			Logger: DBLogger{},
		},
	)
	if nil != err {
		return nil, err
	}

	database := Database{
		DB: db,
	}

	return &database, nil
}

func (db *Database) GetCurrentTransaction() *gorm.DB {
	useDatabase := db.DB
	if nil != db.Transaction {
		useDatabase = db.Transaction
	}
	return useDatabase
}

func (db *Database) Migrate(values ...interface{}) error {
	if err := db.DB.AutoMigrate(values...); nil != err {
		return err
	}
	return nil
}

func (db *Database) CreateDBQuery(model interface{}) *gorm.DB {
	useDatabase := db.GetCurrentTransaction()
	if nil != model {
		useDatabase = useDatabase.Model(model)
	}
	if 0 != echomiddleware.TenantId && "" != echomiddleware.TenantIdField {
		modelType := reflect.ValueOf(model).Type()
		tableName := useDatabase.Config.NamingStrategy.TableName(modelType.Name())
		return useDatabase.Where(fmt.Sprintf("%s.%s = ? OR %[1]s.%[2]s = 0", tableName, echomiddleware.TenantIdField), echomiddleware.TenantId)
	}
	return useDatabase
}

func (db *Database) CreateInsertDBQuery(model interface{}) *gorm.DB {
	useDatabase := db.GetCurrentTransaction()
	if nil != model {
		useDatabase = useDatabase.Model(model)
	}
	return useDatabase
}

func (db *Database) BeginTransaction() {
	if nil != db.Transaction {
		return
	}
	db.Transaction = db.DB.Begin(&sql.TxOptions{
		Isolation: sql.LevelReadCommitted,
	})
}

func (db *Database) CommitTransaction() error {
	if nil != db.Transaction {
		err := db.Transaction.Commit().Error
		db.Transaction = nil
		return err
	}
	return nil
}

func (db *Database) Rollback() error {
	if nil != db.Transaction {
		err := db.Transaction.Rollback().Error
		db.Transaction = nil
		return err
	}
	return nil
}

func (db *Database) RemoveDefaultFields(fields map[string]interface{}) map[string]interface{} {
	// remove default fields
	_, ok := fields["ID"]
	if ok {
		delete(fields, "ID")
	}
	_, ok = fields["id"]
	if ok {
		delete(fields, "id")
	}
	_, ok = fields["CreatedAt"]
	if ok {
		delete(fields, "CreatedAt")
	}
	_, ok = fields["created_at"]
	if ok {
		delete(fields, "created_at")
	}
	_, ok = fields["DeletedAt"]
	if ok {
		delete(fields, "DeletedAt")
	}
	_, ok = fields["deleted_at"]
	if ok {
		delete(fields, "deleted_at")
	}
	_, ok = fields["UpdatedAt"]
	if ok {
		delete(fields, "UpdatedAt")
	}
	_, ok = fields["updated_at"]
	if ok {
		delete(fields, "updated_at")
	}
	return fields
}
