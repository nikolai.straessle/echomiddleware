package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikolai.straessle/echomiddleware/v4"
	"gitlab.com/nikolai.straessle/gotestutils"
	"gorm.io/gorm"
	"reflect"
	"testing"
)

type TestType struct {
	gorm.Model
	Name string
}

func init() {
	gotestutils.Tables = []string{}
}

func TestNewDatabase(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "TestNewDatabase",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock([]gotestutils.Mock{})
			got, err := NewDatabase()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewDatabase() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && nil == got {
				t.Errorf("NewDatabase() got = %v", got)
			}
		})
	}
}

func TestDatabase_GetCurrentTransaction(t *testing.T) {
	type fields struct {
		DB          *gorm.DB
		Transaction *gorm.DB
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "TestDatabase_GetCurrentTransaction",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock([]gotestutils.Mock{})
			database, _ := NewDatabase()
			db := &Database{
				DB:          database.DB,
				Transaction: database.DB,
			}
			if got := db.GetCurrentTransaction(); nil == got {
				t.Errorf("GetCurrentTransaction() = %v", got)
			}
		})
	}
}

func TestDatabase_Migrate(t *testing.T) {
	type args struct {
		values []interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		mocks   []gotestutils.Mock
	}{
		{
			name: "TestDatabase_Migrate",
			args: args{
				values: []interface{}{
					&TestType{},
				},
			},
			wantErr: false,
			mocks: []gotestutils.Mock{
				{
					QueryType: "EXEC",
					Query:     "CREATE TABLE `test_types`",
					Result:    sqlmock.NewResult(1, 1),
				},
			},
		},
		{
			name: "TestDatabase_MigrateError",
			args: args{
				values: []interface{}{
					&TestType{},
				},
			},
			wantErr: true,
			mocks: []gotestutils.Mock{
				{
					QueryType:   "EXEC",
					Query:       "CREATE TABLE `test_types`",
					ReturnError: true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB:          database.DB,
				Transaction: database.DB,
			}
			if err := db.Migrate(tt.args.values...); (err != nil) != tt.wantErr {
				t.Errorf("Migrate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDatabase_CreateDBQuery(t *testing.T) {
	type args struct {
		model    interface{}
		tenantId uint
	}
	tests := []struct {
		name  string
		args  args
		mocks []gotestutils.Mock
	}{
		{
			name: "TestDatabase_CreateDBQuery",
			args: args{
				model:    &TestType{},
				tenantId: 1,
			},
		},
		{
			name: "TestDatabase_CreateDBQueryNoTenantId",
			args: args{
				model:    &TestType{},
				tenantId: 0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			echomiddleware.TenantId = tt.args.tenantId
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if got := db.CreateDBQuery(tt.args.model); nil == got {
				t.Errorf("CreateDBQuery() = %v", got)
			}
		})
	}
}

func TestDatabase_CreateInsertDBQuery(t *testing.T) {
	type args struct {
		model interface{}
	}
	tests := []struct {
		name  string
		args  args
		mocks []gotestutils.Mock
	}{
		{
			name: "TestDatabase_CreateInsertDBQuery",
			args: args{
				model: &TestType{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if got := db.CreateInsertDBQuery(tt.args.model); nil == got {
				t.Errorf("CreateInsertDBQuery() = %v", got)
			}
		})
	}
}

func TestDatabase_BeginTransaction(t *testing.T) {
	type args struct {
		hasTransaction bool
	}
	tests := []struct {
		name  string
		args  args
		mocks []gotestutils.Mock
	}{
		{
			name: "TestDatabase_BeginTransaction",
		},
		{
			name: "TestDatabase_BeginTransactionHasTransaction",
			args: args{
				hasTransaction: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if tt.args.hasTransaction {
				db.Transaction = database.DB
			}
			assert.NotPanics(t, db.BeginTransaction)
			assert.NotNil(t, db.Transaction)
		})
	}
}

func TestDatabase_CommitTransaction(t *testing.T) {
	type args struct {
		hasTransaction bool
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		mocks   []gotestutils.Mock
	}{
		{
			name:    "TestDatabase_CommitTransactionNoTransaction",
			args:    args{},
			wantErr: false,
		},
		{
			name: "TestDatabase_CommitTransaction",
			args: args{
				hasTransaction: true,
			},
			wantErr: false,
			mocks: []gotestutils.Mock{
				{
					QueryType: "BEGIN",
				},
				{
					QueryType: "COMMIT",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if tt.args.hasTransaction {
				db.Transaction = database.DB.Begin()
			}
			if err := db.CommitTransaction(); (err != nil) != tt.wantErr {
				t.Errorf("CommitTransaction() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDatabase_Rollback(t *testing.T) {
	type args struct {
		hasTransaction bool
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		mocks   []gotestutils.Mock
	}{
		{
			name:    "TestDatabase_RollbackNoTransaction",
			args:    args{},
			wantErr: false,
		},
		{
			name: "TestDatabase_Rollback",
			args: args{
				hasTransaction: true,
			},
			wantErr: false,
			mocks: []gotestutils.Mock{
				{
					QueryType: "BEGIN",
				},
				{
					QueryType: "ROLLBACK",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock(tt.mocks)
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if tt.args.hasTransaction {
				db.Transaction = database.DB.Begin()
			}
			if err := db.Rollback(); (err != nil) != tt.wantErr {
				t.Errorf("Rollback() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDatabase_RemoveDefaultFields(t *testing.T) {
	type args struct {
		fields map[string]interface{}
	}
	tests := []struct {
		name string
		args args
		want map[string]interface{}
	}{
		{
			name: "TestDatabase_RemoveDefaultFields",
			args: args{
				fields: map[string]interface{}{"ID": 1, "id": 1, "CreatedAt": "", "created_at": "", "DeletedAt": "", "deleted_at": "", "UpdatedAt": "", "updated_at": ""},
			},
			want: map[string]interface{}{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotestutils.StartDBMock([]gotestutils.Mock{})
			database, _ := NewDatabase()
			db := &Database{
				DB: database.DB,
			}
			if got := db.RemoveDefaultFields(tt.args.fields); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveDefaultFields() = %v, want %v", got, tt.want)
			}
		})
	}
}
