package echomiddleware

import (
	"encoding/json"
	"errors"
	"github.com/casbin/casbin"
	"github.com/dgrijalva/jwt-go"
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/nikolai.straessle/echomiddleware/v5/jwtmiddleware"
	"gitlab.com/nikolai.straessle/echomiddleware/v5/user"
	"gorm.io/gorm"
	"io/ioutil"
	"net/http"
	"strconv"
)

var TenantId = uint(0)
var TenantIdField = "tenant_id"

type Enforcer struct {
	Enforcer *casbin.Enforcer
}

func GetDataFromTokenByKey(ctx echo.Context, key string) (map[string]interface{}, error) {
	if nil != ctx.Get("user") {
		usr := ctx.Get("user").(*jwt.Token)
		claims := usr.Claims.(jwt.MapClaims)
		claimUserToken, ok := claims[key]
		if ok && nil != claimUserToken {
			return claimUserToken.(map[string]interface{}), nil
		}
	}
	return nil, errors.New("key not found")
}

func CheckRole(ctx echo.Context, userKey string, role ...string) error {
	usr, err := GetDataFromTokenByKey(ctx, userKey)
	if nil != err {
		ctx.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	r, ok := usr["role"]
	if nil != usr && ok && 0 < len(role) {
		for _, inRole := range role {
			if inRole == r {
				return nil
			}
		}
	}
	return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
}

func CheckTenantIDFromHTTPHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		tenantId := context.Request().Header.Get("X-Tenant-ID")
		if "" == tenantId {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant_id missing")
		}
		u64, err := strconv.ParseUint(tenantId, 10, 32)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid tenant_id")
		}
		context.Set("tenant_id", uint(u64))
		return next(context)
	}
}

func CheckTenantID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		if 0 != GetCurrentUser(context).ID && GetTenantIdFromContext(context) != GetCurrentUser(context).TenantID {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant id from http-header and jwt not equal")
		}
		return next(context)
	}
}

// Enforce check if users role should have access to the function
// JWT middleware is required to add before
func (e *Enforcer) Enforce(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if 0 == GetCurrentUser(ctx).ID {
			currentUser := GetCurrentUser(ctx)
			currentUser.Role = "anonymous"
			ctx.Set("user", currentUser)
		}
		method := ctx.Request().Method
		path := ctx.Request().URL.Path

		result := e.Enforcer.Enforce(GetCurrentUser(ctx).Role, path, method)

		if result {
			return next(ctx)
		}
		return echo.ErrForbidden
	}
}

func HTTPErrorHandler(err error, c echo.Context) {
	if hub := sentryecho.GetHubFromContext(c); hub != nil {
		hub.CaptureException(err)
	}
	if gorm.ErrRecordNotFound.Error() == err.Error() {
		err = echo.NewHTTPError(http.StatusNotFound, err.Error())
	}
	he, ok := err.(*echo.HTTPError)
	if ok {
		if he.Internal != nil {
			if herr, ok := he.Internal.(*echo.HTTPError); ok {
				he = herr
			}
		}
	} else {
		he = &echo.HTTPError{
			Code:    http.StatusInternalServerError,
			Message: http.StatusText(http.StatusInternalServerError),
		}
	}
	if m, ok := he.Message.(string); ok {
		he.Message = echo.Map{"message": m}
	}

	// Send response
	if !c.Response().Committed {
		if c.Request().Method == http.MethodHead { // Issue #608
			err = c.NoContent(he.Code)
		} else {
			err = c.JSON(he.Code, he.Message)
		}
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"error":  err,
				"url":    c.Request().URL,
				"header": c.Request().Header,
			}).Error("Could not send error response")
		}
	}
}

func SetCurrentUserMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		return SetCurrentUser(next, context)
	}
}

func SetCurrentUser(next echo.HandlerFunc, context echo.Context) error {
	currentUserMap, err := GetDataFromTokenByKey(context, "user")
	if nil != err {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
	}
	if nil != currentUserMap {
		jsonUser, err := json.Marshal(currentUserMap)
		if nil != err {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
		}
		currentUser := user.User{}
		err = json.Unmarshal(jsonUser, &currentUser)
		if nil != err {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
		}
		context.Set("user", currentUser)
	}
	return next(context)
}

func GetCurrentUser(ctx echo.Context) user.User {
	currentUserInterface := ctx.Get("user")
	if nil != currentUserInterface {
		currentUser, ok := currentUserInterface.(user.User)
		if ok {
			return currentUser
		}
	}
	return user.User{
		Role: "anonymous",
	}
}

func PrepareJWTConfig(jwtType string, jwtKey string, publicKeyPath string) (jwtmiddleware.Config, error) {
	jwtConfig := jwtmiddleware.DefaultJWTConfig
	if "secret" == jwtType {
		jwtConfig.SigningKey = jwtKey
	} else if "rsa" == jwtType {
		data, err := ioutil.ReadFile(publicKeyPath)
		if nil != err {
			return jwtmiddleware.Config{}, err
		}
		publicKey, err := jwt.ParseRSAPublicKeyFromPEM(data)
		if nil != err {
			return jwtmiddleware.Config{}, err
		}
		jwtConfig = jwtmiddleware.Config{
			Skipper: func(c echo.Context) bool {
				return false
			},
			ContextKey:    "user",
			SigningKey:    publicKey,
			SigningMethod: "RS256",
			TokenLookup:   "header:" + echo.HeaderAuthorization,
			AllowNoJWT:    true,
		}
	}
	return jwtConfig, nil
}

func InitializeAuth(jwtType, jwtKey, publicKeyPath, casbinModel, casbinPolicy, apiKey string, group *echo.Group) error {
	jwtConfig, err := PrepareJWTConfig(jwtType, jwtKey, publicKeyPath)
	if nil != err {
		return err
	}
	group.Use(jwtmiddleware.JWTWithConfig(jwtConfig))
	group.Use(CheckTenantIDFromHTTPHeader)
	group.Use(SetCurrentUserMiddleware)
	group.Use(CheckTenantID)
	enforcer := Enforcer{
		Enforcer: casbin.NewEnforcer(casbinModel, casbinPolicy),
	}

	group.Use(enforcer.Enforce)

	group.Use(middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		if 0 == GetCurrentUser(c).ID {
			return key == apiKey, nil
		}
		return true, nil
	}))
	return nil
}

func AddRequestContextMiddleware(e *echo.Echo, db *gorm.DB) {
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			db = db.WithContext(context.Request().Context())
			return next(context)
		}
	})
}

func GetTenantIdFromContext(ctx echo.Context) uint {
	tenantIdInterface := ctx.Get("tenant_id")
	if tenantIdInterface == nil {
		return 0
	}
	tenantId, ok := tenantIdInterface.(uint)
	if !ok {
		return 0
	}
	return tenantId
}
