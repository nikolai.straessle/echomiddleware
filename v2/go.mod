module gitlab.com/nikolai.straessle/echomiddleware/v2

go 1.14

require (
	github.com/casbin/casbin v1.9.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.3.1
	github.com/go-test/deep v1.0.5
	github.com/jinzhu/gorm v1.9.11
	github.com/labstack/echo/v4 v4.1.11
	github.com/labstack/gommon v0.3.0
	github.com/selvatico/go-mocket v1.0.7
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	go.elastic.co/apm v1.5.0
	go.elastic.co/apm/module/apmechov4 v1.5.0
	go.elastic.co/apm/module/apmgorm v1.5.0
)
